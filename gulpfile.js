var gulp = require('gulp'),
    pug = require('gulp-pug'),
    htmlmin = require('gulp-htmlmin'),
    sass = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    gulpStylelint = require('gulp-stylelint'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify-es').default,
    concat = require('gulp-concat'),
    mozjpeg = require('imagemin-mozjpeg'),
    pngquant = require('imagemin-pngquant'),
    imagemin = require('gulp-imagemin'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    path = require('path'),
    changed = require('gulp-changed'),
    plumber = require('gulp-plumber'),
    del = require('del'),
    strip = require('gulp-strip-comments'),
    browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

// Static Server
gulp.task('browserSync', function(done) {
    browserSync.init({
        server: "./app",
        index: 'sitemap.html'
    });
    done();
});

// Sass to Css
gulp.task('sass', function (done) {
    gulp.src('dev/styles/sass/main.sass')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(gulpStylelint({
            failAfterError: false,
            reporters: [
                {
                    formatter: 'string',
                    console: true
                }
            ]
        }))
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(autoprefixer('last 99 version'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./app/assets/css'))
        .pipe(browserSync.reload({ stream: true }));
    done();
});

// Stylelint
gulp.task('lint-sass', function lintCssTask(done) {
    gulp.src(['dev/styles/sass/**/*', '!dev/styles/sass/components/_responsive.sass', '!dev/styles/sass/base/_reset.sass'])
        .pipe(plumber())
        .pipe(gulpStylelint({
            failAfterError: false,
            reporters: [
                {
                    formatter: 'string',
                    console: true
                }
            ]
        }))
    done();
});

// Minify Js
gulp.task('minify-js', function (done) {
    gulp.src('dev/javascript/*.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./app/assets/js'))
        .pipe(browserSync.reload({ stream: true }));
    done();
});

// Pug to HTML
gulp.task('views', function (done) {
    gulp.src('dev/views/pages/*.pug')
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        }))
        .pipe(htmlmin({
            collapseWhitespace: true, // delete all spaces
            removeComments: true // delete all comments
        }))
        .pipe(gulp.dest("./app"))
        .pipe(browserSync.reload({ stream: true }));
    done();
});

// SVGSprite
gulp.task('svgstore', function () {
    return gulp.src('dev/views/images/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    removeViewBox: false,
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest('./app/assets/images/'));
});

// Imagemin
gulp.task('images', function (done) {
    gulp.src('dev/views/images/*.+(jpg|jpeg|gif|png)')
        .pipe(changed('app/assets/images'))
        .pipe(imagemin([
            mozjpeg({quality: 80, progressive: true}),
            pngquant({speed: 1, quality: [0.7, 0.9]})
        ],{
            verbose: true
        }))
        .pipe(gulp.dest('app/assets/images'))
    done();
});

// Remove sourcemaps
gulp.task('clean:sourcemaps', function(done) {
    del.sync('app/assets/css/main.min.css.map');
    del.sync('app/assets/js/main.min.js.map');
    done();
})
gulp.task('clean:csscomment', function (done) {
    gulp.src('./app/assets/css/main.min.css')
        .pipe(cleanCSS( {level: { 1: { specialComments: 'all' } } }))
        .pipe(gulp.dest('./app/assets/css'))
    done();
});
gulp.task('clean:jscomment', function (done) {
    gulp.src('./app/assets/js/main.min.js')
        .pipe(strip())
        .pipe(gulp.dest('./app/assets/js'))
    done();
});

// Gulp Watching
gulp.task('watch', function () {
    gulp.watch('dev/views/**/*.pug', gulp.series('views'));
    gulp.watch('dev/styles/sass/**/*.sass', gulp.series('sass'));
    gulp.watch('dev/javascript/*.js', gulp.series('minify-js'));
    gulp.watch('dev/views/images/*.+(jpg|jpeg|gif|png)', gulp.series('images'));
    gulp.watch('dev/views/images/*.svg', gulp.series('svgstore'));
});

// Default
gulp.task('default', gulp.series('sass', 'views', 'minify-js', 'lint-sass', 'images', 'svgstore', gulp.parallel('browserSync', 'watch')));

// Build
gulp.task('build', gulp.series('clean:sourcemaps', 'clean:csscomment', 'clean:jscomment'), function() {
});