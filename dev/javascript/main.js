var acc = document.getElementsByClassName("faq__question-acc");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

$(document).ready( function(){
    $(".site-header__mob").click( function () {
        $('.site-header__mob').toggleClass('is-active');
        $('.site-header__bottom').toggleClass('is-active');
        $('body').toggleClass('hidden');
    });
});




